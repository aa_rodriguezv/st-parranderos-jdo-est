package uniandes.isis2304.parranderos.persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.parranderos.negocio.TipoServicio;

public class SQLTipoServicio {

	
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaParranderos.SQL;

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaParranderos pp;
	
	
	public SQLTipoServicio(PersistenciaParranderos pp) {
		this.pp = pp;
	}
	

	
	/**
	 * 
	 * @param pm
	 * @return
	 */
	public List<TipoServicio> darNombreTodosLosServicios(PersistenceManager pm)
	{	
		String sql = "SELECT * FROM " + pp.darTablaTipoServicio();
		Query q = pm.newQuery(SQL, sql);
		q.setResultClass(TipoServicio.class);
		return (List<TipoServicio>)q.executeList();
	}
	
	/**
	 * 
	 * @param pm
	 * @param nombre
	 * @return
	 */
	public TipoServicio darServicioPorNombre(PersistenceManager pm, String nombre )
	{
		String sql = "SELECT * FROM " + pp.darTablaTipoServicio() + " WHERE NOMBRE = '" + nombre + "'" ;
		Query q = pm.newQuery(SQL, sql);
		q.setResultClass(TipoServicio.class);
		return (TipoServicio)q.executeUnique();
	}
	
}