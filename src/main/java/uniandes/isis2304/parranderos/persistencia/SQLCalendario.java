package uniandes.isis2304.parranderos.persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.parranderos.negocio.Prueba;
import uniandes.isis2304.parranderos.negocio.Pruebahab;

public class SQLCalendario {

	

	
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaParranderos.SQL;

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaParranderos pp;
	
	public SQLCalendario(PersistenciaParranderos pp) {
		this.pp = pp;
	}
	
	
	public void darServiciosMasPopulares(PersistenceManager pm)
	{
		String sql = "DECLARE " + "\n";
		sql += " c_id calendario.Num_semana%type" + "\n";
		sql += " c_fechaini calendario.fecha_inicial%type;" + "\n";
		sql += " c_fechafin calendario.fecha_final%type;" + "\n";
		sql += " CURSOR c_calendario IS" + "\n";
		sql += " SELECT Num_semana,fecha_inicial,fecha_final from calendario;" + "\n";
		sql += "BEGIN" + "\n";
		sql += "	OPEN c_calendario;" + "\n";
		sql += "	LOOP" + "\n";
		sql += "		FETCH c_calendario into c_id, c_fechaini, c_fechafin;" + "\n";
		sql += "  			EXIT WHEN c_calendario%notfound;";
		sql += " -- dbms_output.put_line(c_id || ' ' || c_fechaini || ' ' || c_fechafin);";
		sql += "\n" + "\n";
		sql += "				INSERT INTO PRueba (servicio,semana,cantidad)" + "\n";
		sql += "				select a.Nombre\r\n" + 
				" ,c_id\r\n" + 
				" ,MAX(a.Contador) conteo\r\n" + 
				" from (\r\n" + 
				" \r\n" + 
				" SELECT ts.nombre as Nombre\r\n" + 
				" ,count(ts.nombre) as Contador\r\n" + 
				" FROM CONSUMO C \r\n" + 
				" inner join INVENTARIO_PRODUCTOS IP ON (C.ID_PROD = IP.ID)\r\n" + 
				" INNER JOIN HOTEL_TIPO_SERVICIOS HTP ON (IP.ID_SERVICIO_PROVEEDOR = HTP.ID)\r\n" + 
				" INNER JOIN TIPO_SERVICIOS TS ON (HTP.ID_TIPO_SERVICIOS = TS.ID)\r\n" + 
				" WHERE c.fecha_i >= c_fechaini AND c.fecha_f <= c_fechafin\r\n" + 
				" group by ts.nombre\r\n" + 
				" order by 2 desc) a\r\n" + 
				" WHERE ROWNUM = 1\r\n" + 
				" group by a.Nombre,c_id;";
		sql += "\n" + "\n" + "END LOOP;" + "\n";
		sql += "CLOSE c_calendario"; 
		sql += "END;";

		Query q = pm.newQuery(SQL, sql);
		q.execute();
	}
	
	public void darServiciosMenosPopulares(PersistenceManager pm)
	{
		String sql = "DECLARE " + "\n";
		sql += " c_id calendario.Num_semana%type" + "\n";
		sql += " c_fechaini calendario.fecha_inicial%type;" + "\n";
		sql += " c_fechafin calendario.fecha_final%type;" + "\n";
		sql += " CURSOR c_calendario IS" + "\n";
		sql += " SELECT Num_semana,fecha_inicial,fecha_final from calendario;" + "\n";
		sql += "BEGIN" + "\n";
		sql += "	OPEN c_calendario;" + "\n";
		sql += "	LOOP" + "\n";
		sql += "		FETCH c_calendario into c_id, c_fechaini, c_fechafin;" + "\n";
		sql += "  			EXIT WHEN c_calendario%notfound;";
		sql += " -- dbms_output.put_line(c_id || ' ' || c_fechaini || ' ' || c_fechafin);";
		sql += "\n" + "\n";
		sql += "				INSERT INTO PRueba (servicio,semana,cantidad)" + "\n";
		sql += "				select a.Nombre\r\n" + 
				" ,c_id\r\n" + 
				" ,MAX(a.Contador) conteo\r\n" + 
				" from (\r\n" + 
				" \r\n" + 
				" SELECT ts.nombre as Nombre\r\n" + 
				" ,count(ts.nombre) as Contador\r\n" + 
				" FROM CONSUMO C \r\n" + 
				" inner join INVENTARIO_PRODUCTOS IP ON (C.ID_PROD = IP.ID)\r\n" + 
				" INNER JOIN HOTEL_TIPO_SERVICIOS HTP ON (IP.ID_SERVICIO_PROVEEDOR = HTP.ID)\r\n" + 
				" INNER JOIN TIPO_SERVICIOS TS ON (HTP.ID_TIPO_SERVICIOS = TS.ID)\r\n" + 
				" WHERE c.fecha_i >= c_fechaini AND c.fecha_f <= c_fechafin\r\n" + 
				" group by ts.nombre\r\n" + 
				" order by 2 asc) a\r\n" + 
				" WHERE ROWNUM = 1\r\n" + 
				" group by a.Nombre,c_id;";
		sql += "\n" + "\n" + "END LOOP;" + "\n";
		sql += "CLOSE c_calendario"; 
		sql += "END;";

		Query q = pm.newQuery(SQL, sql);
		q.execute();
	}
	
	
	public void darHabitacionesMasPopulares(PersistenceManager pm)
	{
		String sql = "DECLARE " + "\n";
		sql += " c_id calendario.Num_semana%type" + "\n";
		sql += " c_fechaini calendario.fecha_inicial%type;" + "\n";
		sql += " c_fechafin calendario.fecha_final%type;" + "\n";
		sql += " CURSOR c_calendario IS" + "\n";
		sql += " SELECT Num_semana,fecha_inicial,fecha_final from calendario;" + "\n";
		sql += "BEGIN" + "\n";
		sql += "	OPEN c_calendario;" + "\n";
		sql += "	LOOP" + "\n";
		sql += "		FETCH c_calendario into c_id, c_fechaini, c_fechafin;" + "\n";
		sql += "  			EXIT WHEN c_calendario%notfound;";
		sql += " -- dbms_output.put_line(c_id || ' ' || c_fechaini || ' ' || c_fechafin);";
		sql += "\n" + "\n";
		sql += "				INSERT INTO PRUEBAHAB (NUM_SEMANA,ID_HABITACION,cantidad)" + "\n";
		sql += "				select c_id\r\n" + 
				" ,a.hid\r\n" + 
				" ,MAX(a.Contador) conteo\r\n" + 
				" from (\r\n" + 
				" \r\n" + 
				" SELECT hab.id as hid\r\n" + 
				" ,count(hid) as Contador\r\n" + 
				" FROM RESERVAS R \r\n" + 
				" inner join HABITACION HAB ON (R.ID_HABITACION = HAB.ID)\r\n" + 
				" WHERE R.fecha_i >= c_fechaini AND R.fecha_f <= c_fechafin\r\n" + 
				" group by hid\r\n" + 
				" order by 2 desc) a\r\n" + 
				" WHERE ROWNUM = 1\r\n" + 
				" group by a.hid,c_id;";
		sql += "\n" + "\n" + "END LOOP;" + "\n";
		sql += "CLOSE c_calendario"; 
		sql += "END;";

		Query q = pm.newQuery(SQL, sql);
		 q.execute();
	}
	
	
	public void darHabitacionesMenosPopulares(PersistenceManager pm)
	{
		String sql = "DECLARE " + "\n";
		sql += " c_id calendario.Num_semana%type" + "\n";
		sql += " c_fechaini calendario.fecha_inicial%type;" + "\n";
		sql += " c_fechafin calendario.fecha_final%type;" + "\n";
		sql += " CURSOR c_calendario IS" + "\n";
		sql += " SELECT Num_semana,fecha_inicial,fecha_final from calendario;" + "\n";
		sql += "BEGIN" + "\n";
		sql += "	OPEN c_calendario;" + "\n";
		sql += "	LOOP" + "\n";
		sql += "		FETCH c_calendario into c_id, c_fechaini, c_fechafin;" + "\n";
		sql += "  			EXIT WHEN c_calendario%notfound;";
		sql += " -- dbms_output.put_line(c_id || ' ' || c_fechaini || ' ' || c_fechafin);";
		sql += "\n" + "\n";
		sql += "				INSERT INTO PRUEBAHAB (NUM_SEMANA,ID_HABITACION,cantidad)" + "\n";
		sql += "				select c_id\r\n" + 
				" ,a.hid\r\n" + 
				" ,MAX(a.Contador) conteo\r\n" + 
				" from (\r\n" + 
				" \r\n" + 
				" SELECT hab.id as hid\r\n" + 
				" ,count(hid) as Contador\r\n" + 
				" FROM RESERVAS R \r\n" + 
				" inner join HABITACION HAB ON (R.ID_HABITACION = HAB.ID)\r\n" + 
				" WHERE R.fecha_i >= c_fechaini AND R.fecha_f <= c_fechafin\r\n" + 
				" group by hid\r\n" + 
				" order by 2 asc) a\r\n" + 
				" WHERE ROWNUM = 1\r\n" + 
				" group by a.hid,c_id;";
		sql += "\n" + "\n" + "END LOOP;" + "\n";
		sql += "CLOSE c_calendario"; 
		sql += "END;";

		Query q = pm.newQuery(SQL, sql);
		q.execute();
	}


}
