package uniandes.isis2304.parranderos.persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.parranderos.negocio.Cliente;

public class SQLReserva {

	
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaParranderos.SQL;

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaParranderos pp;
	
	public SQLReserva(PersistenciaParranderos pp) {
		this.pp = pp;
	}
	
	
	
	public List<Cliente> darClientesConAlMenos1ReservaCadaTrimestre(PersistenceManager pm)
	{
		
		String sqlTrimestre1 = "(SELECT CLIENTE.NUM_DOCUMENTO AS IDNUM, CLIENTE.TIPO_DOCUMENTO AS TD FROM" + pp.darTablaCliente();
		sqlTrimestre1 += " INNER JOIN " +  pp.darTablaReserva() + " ON (CLIENTE.NUM_DOCUMENTO = RESERVAS.NUM_DOC AND CLIENTE.TIPO_DOCUMENTO = RESERVAS.TIPO_DOC)";
		sqlTrimestre1 += " WHERE RESERVAS.FECHA_ENTRDADA BETWEEN '01/01/18" + "' AND '" + "31/03/18' RESERVAS.FECHA_SALIDA BETWEEN '01/01/18" + "' AND '" + "31/03/18') AS TRI1";
		
		String sqlTrimestre2 = "(SELECT CLIENTE.NUM_DOCUMENTO AS IDNUM, CLIENTE.TIPO_DOCUMENTO AS TD FROM" + pp.darTablaCliente();
		sqlTrimestre2 += " INNER JOIN " +  pp.darTablaReserva() + " ON (CLIENTE.NUM_DOCUMENTO = RESERVAS.NUM_DOC AND CLIENTE.TIPO_DOCUMENTO = RESERVAS.TIPO_DOC)";
		sqlTrimestre2 += " WHERE RESERVAS.FECHA_ENTRDADA BETWEEN '01/04/18" + "' AND '" + "30/06/18' RESERVAS.FECHA_SALIDA BETWEEN '01/04/18" + "' AND '" + "30/06/18') AS TRI2" ;
		
		String sqlTrimestre3 = "(SELECT CLIENTE.NUM_DOCUMENTO AS IDNUM, CLIENTE.TIPO_DOCUMENTO AS TD FROM" + pp.darTablaCliente();
		sqlTrimestre3 += " INNER JOIN " +  pp.darTablaReserva() + " ON (CLIENTE.NUM_DOCUMENTO = RESERVAS.NUM_DOC AND CLIENTE.TIPO_DOCUMENTO = RESERVAS.TIPO_DOC)";
		sqlTrimestre3 += " WHERE RESERVAS.FECHA_ENTRDADA BETWEEN '01/07/18" + "' AND '" + "30/09/18' RESERVAS.FECHA_SALIDA BETWEEN '01/07/18" + "' AND '" + "30/09/18') AS TRI3"  ;
		
		String sqlTrimestre4 = "(SELECT CLIENTE.NUM_DOCUMENTO AS IDNUM, CLIENTE.TIPO_DOCUMENTO AS TD FROM" + pp.darTablaCliente();
		sqlTrimestre4 += " INNER JOIN " +  pp.darTablaReserva() + " ON (CLIENTE.NUM_DOCUMENTO = RESERVAS.NUM_DOC AND CLIENTE.TIPO_DOCUMENTO = RESERVAS.TIPO_DOC)";
		sqlTrimestre4 += " WHERE RESERVAS.FECHA_ENTRDADA BETWEEN '01/10/18" + "' AND '" + "31/12/18' RESERVAS.FECHA_SALIDA BETWEEN '01/10/18" + "' AND '" + "31/12/18') AS TRI4" ;
		
		
		String clientesBuenosEstancias = "SELECT CLIENTE.* FROM " + pp.darTablaCliente();
		clientesBuenosEstancias += "INNER JOIN " + sqlTrimestre1 + " ON (CLIENTE.NUM_DOCUMENTO = TRI1.IDNUM AND CLIENTE.TIPO_DOCUMENTO = TRI1.TD)" ;
		clientesBuenosEstancias += "INNER JOIN " + sqlTrimestre2 + " ON (CLIENTE.NUM_DOCUMENTO = TRI2.IDNUM AND CLIENTE.TIPO_DOCUMENTO = TRI2.TD)";
		clientesBuenosEstancias += "INNER JOIN " + sqlTrimestre3 + " ON (CLIENTE.NUM_DOCUMENTO = TRI3.IDNUM AND CLIENTE.TIPO_DOCUMENTO = TRI3.TD)" ;
		clientesBuenosEstancias += "INNER JOIN " + sqlTrimestre4 + " ON (CLIENTE.NUM_DOCUMENTO = TRI4.IDNUM AND CLIENTE.TIPO_DOCUMENTO = TRI4.TD)";
		
		
		Query q = pm.newQuery(SQL, clientesBuenosEstancias);
		q.setResultClass(Cliente.class);
		return (List<Cliente>)q.executeList();
	}
	
}
