package uniandes.isis2304.parranderos.persistencia;

import java.util.Date;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.parranderos.negocio.Cliente;

public class SQLConsumo {

	
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaParranderos.SQL;

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaParranderos pp;
	
	public SQLConsumo(PersistenciaParranderos pp) {
		this.pp = pp;
	}
	
	
	/**
	 * 
	 * @param pm
	 * @param fechaInic
	 * @param fechaFin
	 * @return
	 */
	public List<Object> darTop20ServiciosMasPopulares(PersistenceManager pm, String fechaInic, String fechaFin)
	{
		String sql = "(SELECT ID_SERVICIO_PROVEEDOR AS IDSERV, COUNT(20) as TOP FROM "  + pp.darTablaInventario();
		sql += " INNER JOIN " + pp.darTablaConsumo() + " ON INVENTARIO_PRODUCTOS.ID = CONSUMO.ID_PRODUCTO" ;
		sql += " WHERE FECHA_INICIO >= '" + fechaInic + "'AND FECHA_FIN <= '" + fechaFin + "'";
		sql += " GROUP BY ID_SERVICIO_PROVEEDOR";
		sql += " ORDER BY POPULARIDAD DESC) AS SERVICIOSPOPULARES";
		
		String sql2 = "(SELECT HOTEL_TIPO_SERVICIOS.ID AS IDHTS, SERVICIOSPOPULARES.TOP  FROM " + pp.darTablaHotelTipoServicio() + ", " + sql;
		sql2 += "WHERE HOTEL_TIPO_SERVICIOS.ID = SERVICIOSPOPILARES.IDSERV) AS HTS";
		
		String sql3 = "SELECT HTS.IDHTS, TIPO_SERVICIOS.NOMBRE, SERVICIOSPOPULARES.TOP FROM " + pp.darTablaTipoServicio() + ", " + sql2;
		sql3 += "WHERE TIPO_SERVICIOS.ID = HTS.IDHTS";
		
		Query q = pm.newQuery(SQL, sql);
		return q.executeList();
	}
	
	
	/**
	 * 
	 * @param pm
	 * @param idServicio
	 * @param fechaInic
	 * @param fechaFin
	 * @return
	 */
	public List<Object> darInfoUsuariosConsumieronServicio(PersistenceManager pm, long idServicio, String fechaInic, String fechaFin, boolean agruparVecesConsumidas)
	{
		String sqlAgrupar = ", COUNT(TS.ID) AS VECESCONSUMIDAS";
		String groupBy = " GROUP BY CLI.NUM_DOCUMENTO, CLI.NOMBRE, TS.ID, TS.NOMBRE "; 
		
		if(!agruparVecesConsumidas)
		{
			sqlAgrupar = "";
			groupBy = "";
		}
		
		
		String sql1 = "SELECT CLI.NUM_DOCUMENTO, CLI.NOMBRE, TS.ID, TS.NOMBRE " + sqlAgrupar+  "FROM " + pp.darTablaCliente() +" CLI";
		sql1+= " INNER JOIN " + pp.darTablaReserva() + " RES ON (CLI.NUM_DOCUMENTO = RES.NUM_DOC AND CLI.TIPO_DOCUMENTO = RES.TIPO_DOC";
		sql1 += " INNER JOIN " + pp.darTablaConsumo() + " CONS ON (RES.ID_HABITACION = CONS.ID_HAB)";
		sql1 += " INNER JOIN " + pp.darTablaInventario() + " INV ON (CONS.ID_PROD = INV.ID)";
		sql1 += " INNER JOIN " + pp.darTablaHotelTipoServicio() + " HTP ON (INV.ID_SERVICIO_PROVEEDOR = HTP.ID)";
		sql1 += " INNER JOIN " + pp.darTablaTipoServicio() + " TS ON (HTP.ID_TIPO_SERVICIOS = TS.ID)";
		sql1 += " WHERE TS.ID = " + idServicio + " AND CONS.FECHA_I >= '" + fechaInic + "'" + " AND CONS.FECHA_F <= '" + fechaFin + "' " + groupBy ;
		
		
		Query q = pm.newQuery(SQL, sql1);
		return q.executeList();

	}
	
	/**
	 */
	public List<Cliente> darInfoUsuariosNOConsumieronServicio(PersistenceManager pm, long idServicio, String fechaInic, String fechaFin)
	{
		
		String sql1 = "(SELECT CLI.NUM_DOCUMENTO IDC, CLI.TIPO_DOCUMENTO TD" + "FROM " + pp.darTablaCliente() +" CLI";
		sql1+= " INNER JOIN " + pp.darTablaReserva() + " RES ON (CLI.NUM_DOCUMENTO = RES.NUM_DOC AND CLI.TIPO_DOCUMENTO = RES.TIPO_DOC";
		sql1 += " INNER JOIN " + pp.darTablaConsumo() + " CONS ON (RES.ID_HABITACION = CONS.ID_HAB)";
		sql1 += " INNER JOIN " + pp.darTablaInventario() + " INV ON (CONS.ID_PROD = INV.ID)";
		sql1 += " INNER JOIN " + pp.darTablaHotelTipoServicio() + " HTP ON (INV.ID_SERVICIO_PROVEEDOR = HTP.ID)";
		sql1 += " INNER JOIN " + pp.darTablaTipoServicio() + " TS ON (HTP.ID_TIPO_SERVICIOS = TS.ID)";
		sql1 += " WHERE TS.ID = " + idServicio + " AND CONS.FECHA_I >= '" + fechaInic + "'" + " AND CONS.FECHA_F <= '" + fechaFin + "' "  ;
		sql1 += ") AS LOSQUESI";
		
		String sql2 = "SELECT C.* FROM " + pp.darTablaCliente() + " C";
		sql2 += "LEFT JOIN" + sql1 + " ON (C.NUM_DOCUMENTO = LOSQUESI.IDC AND C.TIPO_DOCUMENTO = LOSQUESI.TD)";
		sql2 += "WHERE LOSQUESI.IDC IS NULL";
		
		
		Query q = pm.newQuery(SQL, sql2);
		q.setResultClass(Cliente.class);
		return (List<Cliente>)q.executeList();

	}
	
	
	/**
	 * 
	 * @param pm
	 * @param precio
	 * @return
	 */
	public List<Cliente> darBuenosClientesConsumoCostoso(PersistenceManager pm, int precio)
	{
		String sqlConsumoProductosCostosos = "(SELECT CONSUMO.ID_HABITACION AS CONSDID, CONSUMO.FECHA_INICIO AS FECHIN, CONSUMO.FECHA_FIN AS FECHF FROM " + pp.darTablaConsumo();
		sqlConsumoProductosCostosos += " INNER JOIN " + pp.darTablaInventario() + "ON (CONSUMO.ID_PRODUCTO = INVENTARIO_PRODUCTOS.ID)";
		sqlConsumoProductosCostosos += " WHERE INVENTARIO_PRODUCTOS.PRECIO >= " + precio + ") AS PRODCOSTOSO";
		
		String sqlReservaConsumoCostoso = "(SELECT RESERVA.NUM_DOC AS ND, RESERVA.TIPO_DOC AS TD FROM " + pp.darTablaReserva();
		sqlReservaConsumoCostoso += " INNER JOIN " +  sqlConsumoProductosCostosos + "ON (RESERVA.ID_HABITACION = PRODCOSTOSO.ID_HABITACION ";
		sqlReservaConsumoCostoso += " AND RESERVA.FECHA_ENTRADA <= PRODCOSTOSO.FECHIN AND RESERVA.FECHA_SALIDA >= PRODCOSTOSO.FECHF)) AS RESERVASCONSCOSTOSO";
		sqlReservaConsumoCostoso += "";
		
		String sqlClientesConsumosCostosos = "SELECT CLIENTE.* FROM " + pp.darTablaCliente();
		sqlClientesConsumosCostosos += " INNER JOIN " + sqlReservaConsumoCostoso + " ON (CLIENTE.NUM_DOCUMENTO = RESERVASCONSCOSTOSO.ND AND CLIENTE.TIPO_DOCUMENTO = RESERVASCONSCOSTOSO.TD";
		sqlClientesConsumosCostosos += "HAVING...";
		
		
		Query q = pm.newQuery(SQL, sqlClientesConsumosCostosos);
		q.setResultClass(Cliente.class);
		return (List<Cliente>)q.executeList();
	}
	
	
	public List<Cliente> darBuenosClientesConsumoHorasExtendidas(PersistenceManager pm)
	{
		
		String sql3 = "(SELECT HOTEL_TIPO_SERVICIOS.ID AS ELID" + " FROM " + pp.darTablaHotelTipoServicio();
		sql3 += "INNER JOIN " + pp.darTablaTipoServicio() + " ON HOTEL_TIPO_SERVICIOS.ID_TIPO_SERVICIOS = TIPO_SERVICIO.ID";
		sql3 += "WHERE TIPO_SERVICIO.NOMBRE = 'Spa' OR TIPO_SERVICIO.NOMBRE LIKE 'Salon%'" + ") AS SERVICIOGENERAL";
		
		String sql2 = "(SELECT INVENTARIO.ID AS PRODID" + " FROM " + pp.darTablaInventario();
		sql2 += " INNER JOIN" + sql3 + " ON INVENTARIO_PRODUCTOS.ID_SERVICIO_PROVEEDOR = SERVICIOGENERAL.ELID) AS PRODUCTOSDELSERVICIO ";
		
		String sql1 = "(SELECT ID_HABITACION AS IDH, CONSUMO.FECHA_INICIO AS FEIN, CONSUMO.FECHA_FIN AS FF  FROM " + pp.darTablaConsumo();
		sql1 += " INNER JOIN " + sql2 + " ON (CONSUMO.ID_PRODUCTO = PRODUCTOSDELSERVICIO.PROID)";
		sql1 += "WHERE  (CONSUMO.FECHA_INICIO - CONSUMO.FECHA_FIN) >= 4 ) AS CONS";
		
		String sql0 = "(SELECT RESERVA.NUM_DOC AS ND, RESERVA.TIPO_DOC AS TD " + pp.darTablaReserva();
		sql0 += " INNER JOIN " +  sql1 + " (RESERVAS.ID_HABITACION = CONS.IDH AND RESERVAS.FECHA_ENTRADA = CONS.FEIN AND RESERVAS.RESERVA_SALIDA = CONS.FF)) AS ALMOST";
		
		String sql = "SELECT CLIENTE.* " + pp.darTablaCliente();
		sql += " INNER JOIN " + sql1 + " ON (CLIENTE.NUM_DOCUMENTO = ALMOST.ND AND CLIENTE.TIPO_DOCUMENTO = ALMOST.TD)";
		
		
		Query q = pm.newQuery(SQL, sql);
		q.setResultClass(Cliente.class);
		return (List<Cliente>)q.executeList();
	}
	
	
	
}
