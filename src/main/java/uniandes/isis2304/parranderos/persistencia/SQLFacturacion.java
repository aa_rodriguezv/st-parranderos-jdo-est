package uniandes.isis2304.parranderos.persistencia;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

public class SQLFacturacion {

	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaParranderos.SQL;

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaParranderos pp;
	
	public SQLFacturacion(PersistenciaParranderos pp) {
		this.pp = pp;
	}
	

	/**
	 * Metodo que permite realizar la consulta del consumo de un cliente
	 * @param pm
	 * @param idCliente id del cliente que
	 * @param fechaInic
	 * @param fechaFin
	 * @return
	 */
	public int darElConsumoDeUnCliente(PersistenceManager pm, long idCliente, String tipoDoc, String fechaInic, String fechaFin)
	{
		String sql = "(SELECT * FROM ";
		sql += " WHERE ID_CLIENTE =" + Long.toString(idCliente) + " AND NUM_DOC_CLIENTE = '" + tipoDoc + "'"; 
		sql += " AND FECHA_INICIO >= '" + fechaInic + "'";
		sql += " AND FECHA_FIN <= '" + fechaFin + "') as RESULT_VISITAS_CLIENTE";
		
		String sql2 = "(SELECT ID FROM " +  pp.darTablaConsumo();
		sql2 += "INNER JOIN " + sql;
		sql2 += "ON (RESULT_VISITAS_CLIENTES.ID_HABITACION = CONSUMO.ID_HABITACION AND FECHA_INICIO BETWEEN '" + fechaInic + "' AND '" + fechaFin + "') as CONSUMO_CLIENTE";
		
		String sql3 = "SELECT SUM(VALOR_TOTAL_PAGAR) FROM " + pp.darTablaFacturacion();
		sql3 += "INNER JOIN " + sql2;
		sql3 += "ON CONSUMO_CLIENTE.ID = FACTURACION.ID_CONSUMO";
		
		Query q = pm.newQuery(SQL, sql);
		return (int)q.executeUnique();
	}
}
