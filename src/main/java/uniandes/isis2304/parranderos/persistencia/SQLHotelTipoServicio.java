package uniandes.isis2304.parranderos.persistencia;

public class SQLHotelTipoServicio {

	
	
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaParranderos.SQL;

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaParranderos pp;
	
	public SQLHotelTipoServicio(PersistenciaParranderos pp) {
		this.pp = pp;
	}
	

	public void serviciosCumplenCondiciones()
	{
		String sql = "SELECT * FROM " + pp.darTablaHotelTipoServicio();
	}
}
