package uniandes.isis2304.parranderos.persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.parranderos.negocio.Prueba;
import uniandes.isis2304.parranderos.negocio.Pruebahab;

public class SQLPruebahab {

	
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaParranderos.SQL;

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaParranderos pp;
	
	public SQLPruebahab(PersistenciaParranderos pp) {
		this.pp = pp;
	}
	
	public long truncateTabla (PersistenceManager pm)
	{
        Query q = pm.newQuery(SQL, "TRUNCATE TABLE PRUEBAHAB");
        return (long) q.executeUnique();            
	}
	
	
	
	public List<Pruebahab> darServiciosPopularesPorSemana(PersistenceManager pm)
	{
		String sql = "SELECT * FROM " + pp.darTablaPrueba();
		Query q = pm.newQuery(SQL, sql);
		q.setResultClass(Pruebahab.class);
		return (List<Pruebahab>)q.executeList();
	}
	
}
