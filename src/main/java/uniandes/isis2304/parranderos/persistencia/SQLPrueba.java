package uniandes.isis2304.parranderos.persistencia;

import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;

import uniandes.isis2304.parranderos.negocio.Prueba;

public class SQLPrueba {

	
	
	/* ****************************************************************
	 * 			Constantes
	 *****************************************************************/
	/**
	 * Cadena que representa el tipo de consulta que se va a realizar en las sentencias de acceso a la base de datos
	 * Se renombra acá para facilitar la escritura de las sentencias
	 */
	private final static String SQL = PersistenciaParranderos.SQL;

	/* ****************************************************************
	 * 			Atributos
	 *****************************************************************/
	/**
	 * El manejador de persistencia general de la aplicación
	 */
	private PersistenciaParranderos pp;
	
	public SQLPrueba(PersistenciaParranderos pp) {
		this.pp = pp;
	}
	
	/**
	 * 
	 * @param pm
	 * @param idBebedor
	 * @return
	 */
	public long truncateTabla (PersistenceManager pm)
	{
        Query q = pm.newQuery(SQL, "TRUNCATE TABLE PRUEBA");
        return (long) q.executeUnique();            
	}
	
	
	
	public List<Prueba> darServiciosPopularesPorSemana(PersistenceManager pm)
	{
		String sql = "SELECT * FROM " + pp.darTablaPrueba();
		Query q = pm.newQuery(SQL, sql);
		q.setResultClass(Prueba.class);
		return (List<Prueba>)q.executeList();
	}
	
}
