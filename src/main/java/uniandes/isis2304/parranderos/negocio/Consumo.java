package uniandes.isis2304.parranderos.negocio;

import java.util.Date;

public class Consumo {

	/**
	 * 
	 */
	private long id;
	
	/**
	 * 
	 */
	private long idProducto;
	
	/**
	 * 
	 */
	private long idHabitacion;
	
	/**
	 * 
	 */
	private char cargoHabitacion;
	
	/**
	 * 
	 */
	private Date fechaInicio;
	
	/**
	 * 
	 */
	private Date fechaFin;
	
	
	/**
	 * 
	 * @param pId
	 * @param pIdHab
	 * @param pIdProd
	 * @param pCargo
	 * @param pFechaInic
	 * @param pFechaFin
	 */
	public Consumo(long pId, long pIdHab, long pIdProd, char pCargo, Date pFechaInic, Date pFechaFin)
	{
		id = pId;
		idHabitacion = pIdHab;
		idProducto = pIdProd;
		cargoHabitacion = pCargo;
		fechaInicio = pFechaInic;
		fechaFin = pFechaFin;
		
	}


	/**
	 * 
	 * @return
	 */
	public long getId() {
		return id;
	}


	/**
	 * 
	 * @param id
	 */
	public void setId(long id) {
		this.id = id;
	}


	/**
	 * 
	 * @return
	 */
	public long getIdProducto() {
		return idProducto;
	}


	/**
	 * 
	 * @param idProducto
	 */
	public void setIdProducto(long idProducto) {
		this.idProducto = idProducto;
	}


	/**
	 * 
	 * @return
	 */
	public long getIdHabitacion() {
		return idHabitacion;
	}


	/**
	 * 
	 * @param idHabitacion
	 */
	public void setIdHabitacion(long idHabitacion) {
		this.idHabitacion = idHabitacion;
	}


	/**
	 * 
	 * @return
	 */
	public char getCargoHabitacion() {
		return cargoHabitacion;
	}


	/**
	 * 
	 * @param cargoHabitacion
	 */
	public void setCargoHabitacion(char cargoHabitacion) {
		this.cargoHabitacion = cargoHabitacion;
	}


	/**
	 * 
	 * @return
	 */
	public Date getFechaInicio() {
		return fechaInicio;
	}


	/**
	 * 
	 * @param fechaInicio
	 */
	public void setFechaInicio(Date fechaInicio) {
		this.fechaInicio = fechaInicio;
	}


	/**
	 * 
	 * @return
	 */
	public Date getFechaFin() {
		return fechaFin;
	}


	/**
	 * 
	 * @param fechaFin
	 */
	public void setFechaFin(Date fechaFin) {
		this.fechaFin = fechaFin;
	}
	
	
}
