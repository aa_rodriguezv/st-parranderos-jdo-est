package uniandes.isis2304.parranderos.negocio;

public class TipoServicio {

	/**
	 * 
	 */
	private long id;
	
	/**
	 * 
	 */
	private String nombre;
	
	/**
	 * 
	 * @param pId
	 * @param pNombre
	 */
	public TipoServicio(long pId, String pNombre)
	{
		id = pId;
		nombre = pNombre;
	}

	/**
	 * 
	 * @return
	 */
	public long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public String getNombre() {
		return nombre;
	}

	/**
	 * 
	 * @param nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	
}
