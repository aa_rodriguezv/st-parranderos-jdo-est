package uniandes.isis2304.parranderos.negocio;

public class Facturacion {

	/**
	 * 
	 */
	private long id;
	
	/**
	 * 
	 */
	private long idReserva; 
	
	/**
	 * 
	 */
	private long idConsumo;
	
	/**
	 * 
	 */
	private long idDescuento;
	
	/**
	 * 
	 */
	private int valorTotalPagar;
	
	
	/**
	 * 
	 * @param pId
	 * @param pIdReser
	 * @param pIdCons
	 * @param pIdDes
	 * @param valorTot
	 */
	public Facturacion(long pId, long pIdReser, long pIdCons, long pIdDes, int valorTot)
	{
		id = pId;
		idReserva = pIdReser;
		idConsumo = pIdCons;
		idDescuento = pIdDes;
		valorTotalPagar = valorTot;
	}

	/**
	 * 
	 * @return
	 */
	public long getId() {
		return id;
	}


	/**
	 * 
	 * @param id
	 */
	public void setId(long id) {
		this.id = id;
	}


	/**
	 * 
	 * @return
	 */
	public long getIdReserva() {
		return idReserva;
	}


	/**
	 * 
	 * @param idReserva
	 */
	public void setIdReserva(long idReserva) {
		this.idReserva = idReserva;
	}


	/**
	 * 
	 * @return
	 */
	public long getIdConsumo() {
		return idConsumo;
	}


	/**
	 * 
	 * @param idConsumo
	 */
	public void setIdConsumo(long idConsumo) {
		this.idConsumo = idConsumo;
	}


	/**
	 * 
	 * @return
	 */
	public long getIdDescuento() {
		return idDescuento;
	}


	/**
	 * 
	 * @param idDescuento
	 */
	public void setIdDescuento(long idDescuento) {
		this.idDescuento = idDescuento;
	}


	/**
	 * 
	 * @return
	 */
	public int getValorTotalPagar() {
		return valorTotalPagar;
	}

	
	/**
	 * 
	 * @param valorTotalPagar
	 */
	public void setValorTotalPagar(int valorTotalPagar) {
		this.valorTotalPagar = valorTotalPagar;
	}
	
	
}
