package uniandes.isis2304.parranderos.negocio;

import java.util.Date;

public class HotelTipoServicio {

	/**
	 * 
	 */
	private long id;
	
	/**
	 * 
	 */
	private long idHotel;
	
	/**
	 * 
	 */
	private long idTipoServicio;
	
	/**
	 * 
	 */
	private int capacidad;
	
	/**
	 * 
	 */
	private String descripcion;
	
	/**
	 * 
	 */
	private long idMantenimiento;
	
	/**
	 * 
	 */
	private Date horaApertura;
	
	/**
	 * 
	 */
	private Date horaCierre;
	
	
	/**
	 * 
	 * @param pId
	 * @param pIdHotel
	 * @param pIdTipo
	 * @param pCapacidad
	 * @param pDescripcion
	 * @param pIdMan
	 * @param pHoraA
	 * @param pHoraC
	 */
	public HotelTipoServicio(long pId, long pIdHotel, long pIdTipo, int pCapacidad, String pDescripcion, long pIdMan, Date pHoraA, Date pHoraC)
	{
		id = pId;
		idHotel = pIdHotel;
		idTipoServicio = pIdTipo;
		capacidad = pCapacidad;
		descripcion = pDescripcion;
		idMantenimiento = pIdMan;
		horaApertura = pHoraA;
		horaCierre = pHoraC;
	}


	public long getId() {
		return id;
	}


	/**
	 * 
	 * @param id
	 */
	public void setId(long id) {
		this.id = id;
	}


	/**
	 * 
	 * @return
	 */
	public long getIdHotel() {
		return idHotel;
	}


	/**
	 * 
	 * @param idHotel
	 */
	public void setIdHotel(long idHotel) {
		this.idHotel = idHotel;
	}


	/**
	 * 
	 * @return
	 */
	public long getIdTipoServicio() {
		return idTipoServicio;
	}


	public void setIdTipoServicio(long idTipoServicio) {
		this.idTipoServicio = idTipoServicio;
	}


	/**
	 * 
	 * @return
	 */
	public int getCapacidad() {
		return capacidad;
	}


	/**
	 * 
	 * @param capacidad
	 */
	public void setCapacidad(int capacidad) {
		this.capacidad = capacidad;
	}


	/**
	 * 
	 * @return
	 */
	public String getDescripcion() {
		return descripcion;
	}


	/**
	 * 
	 * @param descripcion
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	/**
	 * 
	 * @return
	 */
	public long getIdMantenimiento() {
		return idMantenimiento;
	}


	/**
	 * 
	 * @param idMantenimiento
	 */
	public void setIdMantenimiento(long idMantenimiento) {
		this.idMantenimiento = idMantenimiento;
	}


	/**
	 * 
	 * @return
	 */
	public Date getHoraApertura() {
		return horaApertura;
	}


	/**
	 * 
	 * @param horaApertura
	 */
	public void setHoraApertura(Date horaApertura) {
		this.horaApertura = horaApertura;
	}


	/**
	 * 
	 * @return
	 */
	public Date getHoraCierre() {
		return horaCierre;
	}


	/**
	 * 
	 * @param horaCierre
	 */
	public void setHoraCierre(Date horaCierre) {
		this.horaCierre = horaCierre;
	}
	
	
	
}
