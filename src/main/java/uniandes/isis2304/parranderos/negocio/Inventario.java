package uniandes.isis2304.parranderos.negocio;

public class Inventario {

	
	/**
	 * 
	 */
	private long id;
	
	/**
	 * 
	 */
	private long idServicioProveedor;
	
	/**
	 * 
	 */
	private String nombre;
	
	/**
	 * 
	 */
	private int precio;
	
	
	/**
	 * 
	 * @param pId
	 * @param pIdServ
	 * @param pNombre
	 * @param pPrecio
	 */
	public Inventario(long pId, long pIdServ, String pNombre, int pPrecio)
	{
		id = pId;
		idServicioProveedor = pIdServ;
		nombre = pNombre;
		precio = pPrecio; 
	}


	/**
	 * 
	 * @return
	 */
	public long getId() {
		return id;
	}


	/**
	 * 
	 * @param id
	 */
	public void setId(long id) {
		this.id = id;
	}


	/**
	 * 
	 * @return
	 */
	public long getIdServicioProveedor() {
		return idServicioProveedor;
	}


	/**
	 * 
	 * @param idServicioProveedor
	 */
	public void setIdServicioProveedor(long idServicioProveedor) {
		this.idServicioProveedor = idServicioProveedor;
	}


	/**
	 * 
	 * @return
	 */
	public String getNombre() {
		return nombre;
	}


	/**
	 * 
	 * @param nombre
	 */
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	/**
	 * 
	 * @return
	 */
	public int getPrecio() {
		return precio;
	}


	/**
	 * 
	 * @param precio
	 */
	public void setPrecio(int precio) {
		this.precio = precio;
	}
	
	
	
}
