package uniandes.isis2304.parranderos.negocio;

public class Pruebahab {

	
	private int numSemana;
	
	private long idHab;
	
	private int cantidad;
	
	public Pruebahab(int num, long id, int pc)
	{
		numSemana = num;
		idHab = id;
		cantidad = pc;
		
	}

	public int getNumSemana() {
		return numSemana;
	}

	public void setNumSemana(int numSemana) {
		this.numSemana = numSemana;
	}

	public long getIdHab() {
		return idHab;
	}

	public void setIdHab(long idHab) {
		this.idHab = idHab;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}
	
	
}
