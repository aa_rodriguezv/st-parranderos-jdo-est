package uniandes.isis2304.parranderos.negocio;

import java.util.Date;

public class Ocupan {
	
	/**
	 * 
	 */
	private long numDocCliente;
	
	/**
	 * 
	 */
	private String tipoDocCliente;
	
	/**
	 * 
	 */
	private long idHabitacion;
	
	/**
	 * 
	 */
	private Date fechaInicial;
	
	/**
	 * 
	 */
	private Date fechaFinal;
	
	
	public Ocupan(int pNumDoc, String pTipo, int pIdHab, Date fechaI, Date fechaF)
	{
		numDocCliente = pNumDoc;
		
		tipoDocCliente = pTipo;
		
		idHabitacion = pIdHab;
		
		fechaInicial = fechaI;
		
		fechaFinal = fechaF;
	}
	
	public long getNumDocCliente() {
		return numDocCliente;
	}


	public void setNumDocCliente(long numDocCliente) {
		this.numDocCliente = numDocCliente;
	}


	public String getTipoDocCliente() {
		return tipoDocCliente;
	}


	public void setTipoDocCliente(String tipoDocCliente) {
		this.tipoDocCliente = tipoDocCliente;
	}


	public long  getIdHabitacion() {
		return idHabitacion;
	}


	public void setIdHabitacion(long idHabitacion) {
		this.idHabitacion = idHabitacion;
	}


	public Date getFechaInicial() {
		return fechaInicial;
	}


	public void setFechaInicial(Date fechaInicial) {
		this.fechaInicial = fechaInicial;
	}


	public Date getFechaFinal() {
		return fechaFinal;
	}


	public void setFechaFinal(Date fechaFinal) {
		this.fechaFinal = fechaFinal;
	}

}
