package uniandes.isis2304.parranderos.negocio;

public class Calendario {

	
	private int numeroDeSemana;
	
	private String sabadoInicial;
	
	private String sabadoFinal;
	
	public Calendario(int numSemana, String pSI, String pSF)
	{
		numeroDeSemana = numSemana;
		sabadoInicial = pSI;
		sabadoFinal = pSF;
	}

	public int getNumeroDeSemana() {
		return numeroDeSemana;
	}

	public void setNumeroDeSemana(int numeroDeSemana) {
		this.numeroDeSemana = numeroDeSemana;
	}

	public String getSabadoInicial() {
		return sabadoInicial;
	}

	public void setSabadoInicial(String sabadoInicial) {
		this.sabadoInicial = sabadoInicial;
	}

	public String getSabadoFinal() {
		return sabadoFinal;
	}

	public void setSabadoFinal(String sabadoFinal) {
		this.sabadoFinal = sabadoFinal;
	}
	
	
	
}
