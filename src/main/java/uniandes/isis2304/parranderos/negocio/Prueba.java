package uniandes.isis2304.parranderos.negocio;

public class Prueba {

	
	private String nombre;
	
	private int cantidad;
	
	private int idSemana;
	
	
	public Prueba(String pNombreServicio, int pCantidad, int pID)
	{
		nombre = pNombreServicio;
		cantidad = pCantidad;
		idSemana = pID;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public int getCantidad() {
		return cantidad;
	}


	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}


	public int getIdSemana() {
		return idSemana;
	}


	public void setIdSemana(int idSemana) {
		this.idSemana = idSemana;
	}
	
	
	
	
	
}
